﻿using System;
using System.Windows.Forms;

namespace Lab1
{
    public partial class CalculatorDigitButton : CalculatorButton
    {


        public CalculatorDigitButton() : base()
        {
            InitializeComponent();
        }
        
        public CalculatorDigitButton(int value) : this()
        {
            SetDigitValue(value);
        }

        public void SetDigitValue(int value)
        {
            ButtonValue = value.ToString();
            Text = ButtonValue;
        }

        protected override bool IsApplicable(InputFieldState state)
        {
            int thisValue = DigitValue();
            InputFieldState.InputFieldToken lastToken = state.LastToken();

            // forbid zero as first digit
            return (thisValue == 0) ? (lastToken != InputFieldState.InputFieldToken.Empty) : true;
        }

        protected override InputFieldState NextState(InputFieldState prevState)
        {
            return prevState.Advance(Char.Parse(ButtonValue));
        }

        protected override void OnPaint(PaintEventArgs pe)
        {
            base.OnPaint(pe);
        }

        private int DigitValue()
        {
            return Int32.Parse(ButtonValue);
        }
    }
}
