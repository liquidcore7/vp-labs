﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab1
{
    public partial class CalculatorButton : Button
    {
        protected string ButtonValue { get; set; }

        protected virtual bool IsApplicable(InputFieldState state) 
        {
            return false;
        }
        protected virtual InputFieldState NextState(InputFieldState prevState)
        {
            return prevState;
        }


        public CalculatorButton()
        {
            InitializeComponent();
            ButtonValue = Tag?.ToString();
            Text = ButtonValue;
        }

        protected override void OnPaint(PaintEventArgs pe)
        {
            base.OnPaint(pe);
        }

        public void SendInput(NumberInput input)
        {
            char key = ButtonValue.First();
            input.KeyPressed(key);
        }

    }
}
