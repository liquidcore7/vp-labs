﻿namespace Lab1
{
    partial class MainWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.digitGridLayout = new System.Windows.Forms.TableLayoutPanel();
            this.button1 = new Lab1.CalculatorDigitButton();
            this.button2 = new Lab1.CalculatorDigitButton();
            this.button3 = new Lab1.CalculatorDigitButton();
            this.button4 = new Lab1.CalculatorDigitButton();
            this.button5 = new Lab1.CalculatorDigitButton();
            this.button6 = new Lab1.CalculatorDigitButton();
            this.button7 = new Lab1.CalculatorDigitButton();
            this.button8 = new Lab1.CalculatorDigitButton();
            this.button9 = new Lab1.CalculatorDigitButton();
            this.side_a = new Lab1.NumberInput();
            this.side_b = new Lab1.NumberInput();
            this.side_c = new Lab1.NumberInput();
            this.button0 = new Lab1.CalculatorDigitButton();
            this.button_decimal = new Lab1.CalculatorDecimalPlaceButton();
            this.button_c = new Lab1.CalculatorReturnButton(this.components);
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.areaResult = new System.Windows.Forms.TextBox();
            this.triangleDrawingBox = new System.Windows.Forms.PictureBox();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.digitGridLayout.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.triangleDrawingBox)).BeginInit();
            this.tableLayoutPanel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // digitGridLayout
            // 
            this.digitGridLayout.AutoSize = true;
            this.digitGridLayout.ColumnCount = 3;
            this.digitGridLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.digitGridLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33334F));
            this.digitGridLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33334F));
            this.digitGridLayout.Controls.Add(this.button1, 0, 3);
            this.digitGridLayout.Controls.Add(this.button2, 1, 3);
            this.digitGridLayout.Controls.Add(this.button3, 2, 3);
            this.digitGridLayout.Controls.Add(this.button4, 0, 2);
            this.digitGridLayout.Controls.Add(this.button5, 1, 2);
            this.digitGridLayout.Controls.Add(this.button6, 2, 2);
            this.digitGridLayout.Controls.Add(this.button7, 0, 1);
            this.digitGridLayout.Controls.Add(this.button8, 1, 1);
            this.digitGridLayout.Controls.Add(this.button9, 2, 1);
            this.digitGridLayout.Controls.Add(this.side_a, 0, 0);
            this.digitGridLayout.Controls.Add(this.side_b, 1, 0);
            this.digitGridLayout.Controls.Add(this.side_c, 2, 0);
            this.digitGridLayout.Controls.Add(this.button0, 1, 4);
            this.digitGridLayout.Controls.Add(this.button_decimal, 2, 4);
            this.digitGridLayout.Controls.Add(this.button_c, 0, 4);
            this.digitGridLayout.Dock = System.Windows.Forms.DockStyle.Fill;
            this.digitGridLayout.Location = new System.Drawing.Point(3, 3);
            this.digitGridLayout.Name = "digitGridLayout";
            this.digitGridLayout.RowCount = 5;
            this.digitGridLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.digitGridLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.digitGridLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.digitGridLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.digitGridLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.digitGridLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.digitGridLayout.Size = new System.Drawing.Size(474, 444);
            this.digitGridLayout.TabIndex = 0;
            // 
            // button1
            // 
            this.button1.AutoSize = true;
            this.button1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button1.Location = new System.Drawing.Point(3, 267);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(151, 82);
            this.button1.TabIndex = 12;
            this.button1.Tag = "1";
            this.button1.Text = "1";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.AutoSize = true;
            this.button2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button2.Location = new System.Drawing.Point(160, 267);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(152, 82);
            this.button2.TabIndex = 13;
            this.button2.Tag = "2";
            this.button2.Text = "2";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // button3
            // 
            this.button3.AutoSize = true;
            this.button3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button3.Location = new System.Drawing.Point(318, 267);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(153, 82);
            this.button3.TabIndex = 14;
            this.button3.Tag = "3";
            this.button3.Text = "3";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // button4
            // 
            this.button4.AutoSize = true;
            this.button4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button4.Location = new System.Drawing.Point(3, 179);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(151, 82);
            this.button4.TabIndex = 15;
            this.button4.Tag = "4";
            this.button4.Text = "4";
            this.button4.UseVisualStyleBackColor = true;
            // 
            // button5
            // 
            this.button5.AutoSize = true;
            this.button5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button5.Location = new System.Drawing.Point(160, 179);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(152, 82);
            this.button5.TabIndex = 16;
            this.button5.Tag = "5";
            this.button5.Text = "5";
            this.button5.UseVisualStyleBackColor = true;
            // 
            // button6
            // 
            this.button6.AutoSize = true;
            this.button6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button6.Location = new System.Drawing.Point(318, 179);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(153, 82);
            this.button6.TabIndex = 17;
            this.button6.Tag = "6";
            this.button6.Text = "6";
            this.button6.UseVisualStyleBackColor = true;
            // 
            // button7
            // 
            this.button7.AutoSize = true;
            this.button7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button7.Location = new System.Drawing.Point(3, 91);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(151, 82);
            this.button7.TabIndex = 18;
            this.button7.Tag = "7";
            this.button7.Text = "7";
            this.button7.UseVisualStyleBackColor = true;
            // 
            // button8
            // 
            this.button8.AutoSize = true;
            this.button8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button8.Location = new System.Drawing.Point(160, 91);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(152, 82);
            this.button8.TabIndex = 19;
            this.button8.Tag = "8";
            this.button8.Text = "8";
            this.button8.UseVisualStyleBackColor = true;
            // 
            // button9
            // 
            this.button9.AutoSize = true;
            this.button9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button9.Location = new System.Drawing.Point(318, 91);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(153, 82);
            this.button9.TabIndex = 20;
            this.button9.Tag = "9";
            this.button9.Text = "9";
            this.button9.UseVisualStyleBackColor = true;
            // 
            // side_a
            // 
            this.side_a.AutoSize = true;
            this.side_a.Dock = System.Windows.Forms.DockStyle.Fill;
            this.side_a.Location = new System.Drawing.Point(3, 3);
            this.side_a.Name = "side_a";
            this.side_a.Size = new System.Drawing.Size(151, 82);
            this.side_a.TabIndex = 0;
            // 
            // side_b
            // 
            this.side_b.AutoSize = true;
            this.side_b.Dock = System.Windows.Forms.DockStyle.Top;
            this.side_b.Location = new System.Drawing.Point(160, 3);
            this.side_b.Name = "side_b";
            this.side_b.Size = new System.Drawing.Size(152, 45);
            this.side_b.TabIndex = 1;
            // 
            // side_c
            // 
            this.side_c.AutoSize = true;
            this.side_c.Dock = System.Windows.Forms.DockStyle.Top;
            this.side_c.Location = new System.Drawing.Point(318, 3);
            this.side_c.Name = "side_c";
            this.side_c.Size = new System.Drawing.Size(153, 45);
            this.side_c.TabIndex = 2;
            // 
            // button0
            // 
            this.button0.AutoSize = true;
            this.button0.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button0.Location = new System.Drawing.Point(160, 355);
            this.button0.Name = "button0";
            this.button0.Size = new System.Drawing.Size(152, 86);
            this.button0.TabIndex = 0;
            this.button0.Tag = "0";
            this.button0.Text = "0";
            this.button0.UseVisualStyleBackColor = true;
            // 
            // button_decimal
            // 
            this.button_decimal.AutoSize = true;
            this.button_decimal.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button_decimal.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F);
            this.button_decimal.Location = new System.Drawing.Point(318, 355);
            this.button_decimal.Name = "button_decimal";
            this.button_decimal.Size = new System.Drawing.Size(153, 86);
            this.button_decimal.TabIndex = 21;
            this.button_decimal.Tag = ".";
            this.button_decimal.Text = ".";
            this.button_decimal.UseVisualStyleBackColor = true;
            // 
            // button_c
            // 
            this.button_c.AutoSize = true;
            this.button_c.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button_c.Location = new System.Drawing.Point(3, 355);
            this.button_c.Name = "button_c";
            this.button_c.Size = new System.Drawing.Size(151, 86);
            this.button_c.TabIndex = 11;
            this.button_c.Tag = "action_erase";
            this.button_c.Text = "C";
            this.button_c.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.AutoSize = true;
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.areaResult, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.triangleDrawingBox, 0, 2);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(483, 3);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.03773F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 83.96227F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(314, 444);
            this.tableLayoutPanel1.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Location = new System.Drawing.Point(3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(308, 20);
            this.label1.TabIndex = 2;
            this.label1.Text = "Площа:";
            // 
            // areaResult
            // 
            this.areaResult.Dock = System.Windows.Forms.DockStyle.Top;
            this.areaResult.Location = new System.Drawing.Point(3, 23);
            this.areaResult.Name = "areaResult";
            this.areaResult.ReadOnly = true;
            this.areaResult.Size = new System.Drawing.Size(308, 22);
            this.areaResult.TabIndex = 1;
            // 
            // triangleDrawingBox
            // 
            this.triangleDrawingBox.Location = new System.Drawing.Point(3, 90);
            this.triangleDrawingBox.Name = "triangleDrawingBox";
            this.triangleDrawingBox.Size = new System.Drawing.Size(308, 351);
            this.triangleDrawingBox.TabIndex = 3;
            this.triangleDrawingBox.TabStop = false;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Location = new System.Drawing.Point(-191, 298);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(184, 98);
            this.tableLayoutPanel2.TabIndex = 2;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.AutoSize = true;
            this.tableLayoutPanel3.ColumnCount = 2;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 60F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel3.Controls.Add(this.digitGridLayout, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.tableLayoutPanel1, 1, 0);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 1;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(800, 450);
            this.tableLayoutPanel3.TabIndex = 3;
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.tableLayoutPanel3);
            this.Controls.Add(this.tableLayoutPanel2);
            this.Name = "MainWindow";
            this.Text = "Triangle area calculator";
            this.digitGridLayout.ResumeLayout(false);
            this.digitGridLayout.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.triangleDrawingBox)).EndInit();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel digitGridLayout;
        private CalculatorReturnButton button_c = new CalculatorReturnButton();
        private CalculatorDigitButton button0 = new CalculatorDigitButton(0);
        private CalculatorDigitButton button1 = new CalculatorDigitButton(1);
        private CalculatorDigitButton button2 = new CalculatorDigitButton(2);
        private CalculatorDigitButton button3 = new CalculatorDigitButton(3);
        private CalculatorDigitButton button4 = new CalculatorDigitButton(4);
        private CalculatorDigitButton button5 = new CalculatorDigitButton(5);
        private CalculatorDigitButton button6 = new CalculatorDigitButton(6);
        private CalculatorDigitButton button7 = new CalculatorDigitButton(7);
        private CalculatorDigitButton button8 = new CalculatorDigitButton(8);
        private CalculatorDigitButton button9 = new CalculatorDigitButton(9);
        private CalculatorDecimalPlaceButton button_decimal = new CalculatorDecimalPlaceButton();
        private NumberInput side_c;
        private NumberInput side_b;
        private NumberInput side_a;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TextBox areaResult;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox triangleDrawingBox;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
    }
}

