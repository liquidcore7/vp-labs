﻿using System.Linq;
using System.Windows.Forms;


namespace Lab1
{
    public struct InputFieldState
    {

        public enum InputFieldToken
        {
            Empty, Zero, NonZero, Comma, Return, Invalid
        }

        public static Keys[] TokenKeyCodes = new Keys[]
        {
            Keys.NumPad0,
            Keys.NumPad1,
            Keys.NumPad2,
            Keys.NumPad3,
            Keys.NumPad4,
            Keys.NumPad5,
            Keys.NumPad6,
            Keys.NumPad7,
            Keys.NumPad8,
            Keys.NumPad9,
            Keys.Back,
            Keys.Return,
            Keys.D0,
            Keys.D1,
            Keys.D2,
            Keys.D3,
            Keys.D4,
            Keys.D5,
            Keys.D6,
            Keys.D7,
            Keys.D8,
            Keys.D9,
            Keys.Decimal,
            Keys.Oemcomma,
            Keys.Separator
        };

        private static InputFieldToken AsToken(char c)
        {
            switch (c)
            {
                case '0': return InputFieldToken.Zero;
                case '.': return InputFieldToken.Comma;
                case '\r': return InputFieldToken.Return;
                default:
                    if (c >= '1' && c <= '9') return InputFieldToken.NonZero;
                    else return InputFieldToken.Invalid;
            }
        }

        private bool HasDecimalPlace()
        {
            return Value.Select(AsToken).Contains(InputFieldToken.Comma);
        }

        public bool IsEmpty()
        {
            return Value.Length == 0;
        }

        public string Value { get; set; }

        public InputFieldState(string fromValue)
        {
            this.Value = fromValue;
        }

        public InputFieldToken LastToken()
        {
            if (Value?.Length == 0) return InputFieldToken.Empty;
            else
            {
                char lastCharacter = Value.Last();
                return AsToken(lastCharacter);
            }
        }

        public bool CanAppend(char candidate)
        {
            var candidateToken = AsToken(candidate);

            switch (candidateToken)
            {
                case InputFieldToken.Comma: return !HasDecimalPlace() && !IsEmpty();
                case InputFieldToken.Zero:
                    return Value.Length == 1 ? AsToken(Value.First()) != InputFieldToken.Zero : true;
                case InputFieldToken.NonZero:
                    return true;
                case InputFieldToken.Return:
                    return !IsEmpty();
                default: return false;
            }
        }

        public bool NaiveCanAppend(Keys key)
        {
            return InputFieldState.TokenKeyCodes.Contains(key);
        }

        public InputFieldState Advance(char appended)
        {
            if (CanAppend(appended))
            {
                var token = AsToken(appended);
                if (token == InputFieldToken.Return)
                {
                    Value = Value.Substring(0, Value.Length - 1);
                } else
                {
                    Value = $"{Value}{appended}";
                }
            }
            return this;
        }
    }
}
