﻿using System;


namespace Lab1
{
    public struct Triangle
    {
        public readonly double a;
        public readonly double b;
        public readonly double c;

        private static double AngleC(double a, double b, double c)
        {
            return Math.Acos((a * a + b * b - c * c) / (2.0 * a * b));

        }

        public Triangle(double a, double b, double c)
        {
            this.a = a;
            this.b = b;
            this.c = c;
        }

        public double Area()
        {
            double halfPerimeter = (a + b + c) / 2.0;
            return Math.Sqrt(halfPerimeter * (halfPerimeter - a) * (halfPerimeter - b) * (halfPerimeter - c));
        }

        public Triangle AscendingSides()
        {
            double[] sides = { a, b, c };
            Array.Sort(sides);
            return new Triangle(sides[0], sides[1], sides[2]);
        }

        public bool IsObtuse()
        {
            return a * a + b * b < c * c;
        }

        public double TopAngle()
        {
            return AngleC(c, a, b);
        }

        public double BottomAngle()
        {
            return AngleC(c, b, a);
        }

        public Triangle Scaled(double factor)
        {
            return new Triangle(a * factor, b * factor, c * factor);
        }

        public double Height()
        {
            return Math.Sin(TopAngle()) * a;

        }
    }
}
