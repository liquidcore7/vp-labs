﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1
{
    public partial class CalculatorReturnButton : CalculatorButton
    {

        private static char CarriageReturn = '\r';

        public CalculatorReturnButton() : base()
        {
            InitializeComponent();
            ButtonValue = CarriageReturn.ToString();
            Text = "C";
        }

        protected override bool IsApplicable(InputFieldState state)
        {
            return state.LastToken() != InputFieldState.InputFieldToken.Empty;
        }

        protected override InputFieldState NextState(InputFieldState prevState)
        {
            return prevState.Advance(CarriageReturn);
        }

        public CalculatorReturnButton(IContainer container) : this()
        {
            container.Add(this);
        }
    }
}
