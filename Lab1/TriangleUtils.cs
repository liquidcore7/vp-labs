﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace Lab1
{
    public class TriangleUtils
    {
        private readonly ApplicationSettings config;

        public TriangleUtils(ApplicationSettings config)
        {
            this.config = config;
        }

        public String CalculateArea(Triangle tr)
        {
            return String.Format("{0:0.##}", tr.Area());
        }

        public void DrawTriangle(Triangle tr, PictureBox picture)
        { 
            Triangle sortedSides = tr.AscendingSides();

            double width = sortedSides.Height();
            double height = sortedSides.c;

            double verticalScale = (picture.Height - 10) / height;
            double horizontalScale = (picture.Width - 10) / width;
            double scale = Math.Min(verticalScale, horizontalScale);

            Triangle scaled = sortedSides.Scaled(scale);
            width = scaled.Height();

            Pen pen = new Pen(config.color, config.lineWidth);

            using (Graphics g = picture.CreateGraphics()) // Use the CreateGraphics method to create a graphic and draw on the picture box. Use using in order to free the graphics resources.
            {
                g.Clear(picture.BackColor);
                float yIntercept = (float) Math.Sqrt(scaled.b * scaled.b - width * width);
                g.DrawLine(pen, 5, 5, 5, 5 + (float) scaled.c);
                g.DrawLine(pen, 5, 5, 5 + (float) width, 5 + yIntercept);
                g.DrawLine(pen, 5, 5 + (float)scaled.c, 5 + (float)width, 5 + yIntercept);
            };
        }


    }
}
