﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1
{
    public struct ApplicationSettings
    {
        public readonly int Precision;
        public readonly Color color;
        public readonly int lineWidth;

        public ApplicationSettings(int precision, Color color, int lineWidth)
        {
            Precision = precision;
            this.color = color;
            this.lineWidth = lineWidth;
        }

        public static ApplicationSettings Default()
        { 
            return new ApplicationSettings(4, Color.Red, 2);
        }
    }
}
