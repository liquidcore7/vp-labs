﻿namespace Lab1
{
    partial class NumberInput
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label = new System.Windows.Forms.Label();
            this.numberInputBox = new System.Windows.Forms.TextBox();
            this.invalidNumFormat = new System.Windows.Forms.ErrorProvider(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.invalidNumFormat)).BeginInit();
            this.SuspendLayout();
            // 
            // label
            // 
            this.label.AutoSize = true;
            this.label.Location = new System.Drawing.Point(3, 0);
            this.label.Name = "label";
            this.label.Size = new System.Drawing.Size(38, 17);
            this.label.TabIndex = 1;
            this.label.Text = "label";
            // 
            // numberInputBox
            // 
            this.numberInputBox.Location = new System.Drawing.Point(0, 20);
            this.numberInputBox.Name = "numberInputBox";
            this.numberInputBox.Size = new System.Drawing.Size(198, 22);
            this.numberInputBox.TabIndex = 0;
            // 
            // invalidNumFormat
            // 
            this.invalidNumFormat.ContainerControl = this;
            // 
            // NumberInput
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.AutoSize = true;
            this.Controls.Add(this.numberInputBox);
            this.Controls.Add(this.label);
            this.Name = "NumberInput";
            this.Size = new System.Drawing.Size(201, 45);
            ((System.ComponentModel.ISupportInitialize)(this.invalidNumFormat)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TextBox numberInputBox;
        private System.Windows.Forms.Label label;
        private System.Windows.Forms.ErrorProvider invalidNumFormat;
    }
}
