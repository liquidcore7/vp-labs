﻿using System;
using System.Windows.Forms;

namespace Lab1
{
    public partial class NumberInput : UserControl
    {
        private InputFieldState state = new InputFieldState("");
        public InputFieldState State { get => state; }

        public delegate void OnNumberChanged(double number, NumberInput inputRef);
        private OnNumberChanged onNumberChanged;

        private bool validNextTokenEntered = false;

        public NumberInput()
        {
            InitializeComponent();

            numberInputBox.Text = State.Value;
            numberInputBox.Enabled = true;

            numberInputBox.KeyDown += numberInputBoxKeyDown;
            numberInputBox.KeyPress += numberInputBoxKeyPress;
        }

        private void numberInputBoxKeyDown(object inputBox, KeyEventArgs args)
        {
            bool canBePressed = state.NaiveCanAppend(args.KeyCode);
            this.validNextTokenEntered = canBePressed;
            args.SuppressKeyPress = !canBePressed;
            args.Handled = true;
        }

        private void numberInputBoxKeyPress(object inputBox, KeyPressEventArgs args)
        {
            if (this.validNextTokenEntered)
            {
                bool stateChanged = KeyPressed(args.KeyChar);
                this.validNextTokenEntered = false;
                args.Handled = stateChanged;
            }
            this.validNextTokenEntered = false;
            args.Handled = true;
        }

        public bool KeyPressed(char key)
        {
            bool applicable = state.CanAppend(key);
            if (applicable) {
                state = state.Advance(key);
                numberInputBox.Text = state.Value;

                double? newNumber = Number();
                if (onNumberChanged != null && newNumber != null)
                {
                    onNumberChanged.Invoke(newNumber.Value, this);
                }
            }
            return applicable;
        }

        public NumberInput(string labelText) : this()
        {
            label.Text = labelText;
        }

        public void RegisterOnClick(EventHandler handler)
        {
            Click += handler;
            label.Click += new System.EventHandler((label, args) => { handler.Invoke(this, args); });
            numberInputBox.Click += new System.EventHandler((inputBox, args) => { handler.Invoke(this, args); });
        }

        public void RegisterOnChange(OnNumberChanged handler)
        {
            onNumberChanged = handler;
        }

        public void SetLabel(string label)
        {
            this.label.Text = label;
        }

        public double? Number()
        {
            double result;
            if (Double.TryParse(State.Value, out result))
            {
                return result;
            }
            return null;
        }

    }
}
