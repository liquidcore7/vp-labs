﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab1
{
    public partial class CalculatorDecimalPlaceButton : CalculatorButton
    {
        private char DecimalPlaceValue = '.';

        public CalculatorDecimalPlaceButton() : base()
        {
            InitializeComponent();
            ButtonValue = this.DecimalPlaceValue.ToString();
            Text = ButtonValue;
        }

        protected override bool IsApplicable(InputFieldState state)
        {
            return state.LastToken() != InputFieldState.InputFieldToken.Empty;
        }

        protected override InputFieldState NextState(InputFieldState prevState)
        {
            return prevState.Advance(this.DecimalPlaceValue);
        }

        protected override void OnPaint(PaintEventArgs pe)
        {
            base.OnPaint(pe);
        }
    }
}
