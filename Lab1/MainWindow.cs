﻿using System;
using System.Windows.Forms;
using System.Linq;

namespace Lab1
{
    public partial class MainWindow : Form
    {
        private CalculatorButton[] buttons;
        private NumberInput[] inputs;
        private NumberInput activeInput;

        private TriangleUtils triangleUtils = new TriangleUtils(
            ApplicationSettings.Default()
        );
        public MainWindow()
        {
            InitializeComponent();
            this.buttons = new CalculatorButton[12] {
                button0,
                button1,
                button2,
                button3,
                button4,
                button5,
                button6,
                button7,
                button8,
                button9,
                button_decimal,
                button_c
            };
            for (int i = 0; i < buttons.Length; ++i)
            {
                buttons[i].Click += new System.EventHandler(buttonClick);
                if (i < 10)
                {
                    CalculatorDigitButton digit = buttons[i] as CalculatorDigitButton;
                    digit.SetDigitValue(i);
                }
            }
            this.inputs = new NumberInput[3] {
                side_a,
                side_b,
                side_c
            };
            for (int i = 0; i < this.inputs.Length; ++i)
            {
                this.inputs[i].RegisterOnClick(new System.EventHandler(numberInputClick));
                this.inputs[i].RegisterOnChange(newNumberAppeared);

                char label = (char) ('a' + i);
                this.inputs[i].SetLabel($"Сторона {label}:");
            }
            triangleDrawingBox.Resize += new EventHandler(onPictureBoxResize);
        }

        private void buttonClick(object sender, EventArgs e)
        {
            if (activeInput == null) return;
            (sender as CalculatorButton)?.SendInput(activeInput);
        }
        private void numberInputClick(object sender, EventArgs e)
        {
            NumberInput selected = sender as NumberInput;
            if (selected != null)
            {
                activeInput = selected;
            }
        }

        private Triangle? GetTriangle()
        {
            var sides = inputs
                .Select(input => input.Number())
                .Where(num => num.HasValue)
                .Select(maybe => maybe.Value)
                .ToArray();

            if (sides.Length == 3) return new Triangle(
                sides[0],
                sides[1],
                sides[2]);
            else return null;
        }

        private void newNumberAppeared(double number, NumberInput inputRef)
        {
            Triangle? triangle = GetTriangle();

            if (triangle != null)
            {
                areaResult.Text = triangleUtils.CalculateArea(triangle.Value);
                triangleUtils.DrawTriangle(triangle.Value, triangleDrawingBox);
            }
        }

        private void onPictureBoxResize(object sender, EventArgs args)
        {
            Triangle? triangle = GetTriangle();
            if (triangle != null)
            {
                triangleUtils.DrawTriangle(triangle.Value, triangleDrawingBox);
            }
        }
    }
}
